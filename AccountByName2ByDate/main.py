#!/bin/python
#-*- coding=utf-8 -*-
__author__ = 'dongxu'

# 使用说明：
# 首先收集 各个人的报账信息，格式如工程示例中所示，sheet名必须为"按人名报账"
# 在config.py中指定餐饮报账的 行起始和结束位置 dinner_row_start和dinner_row_end
# 在config.py中指定按照个人excel
# 运行该文件后，会在源excel同级目录下生成一个同名，但后边跟了一个-bydate的excel文件，这里就是按照时间维度来统计的报账信息了

from config import *
import xlrd
import xlwt
import os

# 将日期字段转换为日期列表
# 日期字段的格式为：
# 2015-03-01,02,03,04\n
# 2015-04-02,05,06,22,30\n
# 2015-05-03,05,11,22
def format_date_col(date_str):
    result = []
    dates_line = dates.strip().split("\n")
    for date_line in dates_line:
        year_month_days = date_line.split("-")
        year = year_month_days[0].strip()
        month = year_month_days[1].strip()
        if len(month) == 1:
            month = "0" + month
        days = year_month_days[2].split(",")
        for day in days:
            rl_days = day.split(u"，")
            for rl_day in rl_days:
                if len(rl_day) == 1:
                    rl_day = "0" + rl_day
                date = "%s-%s-%s" % (year, month, rl_day.strip())
                result.append(date)
    return result


# 将字符串数组通过","字符连接起来成为一个字符串
def cat_str_list(str_list):
    result_str = ""
    idx = 1
    for _str in str_list:
        result_str += _str
        if len(str_list) == idx:
            break
        result_str += ","
        idx += 1
    return result_str


if __name__ == "__main__":
    workbook = xlrd.open_workbook(excel_file)
    table_by_name = workbook.sheet_by_name(u"按人名报账")

    # 报账类型为加班餐的 ，按日期维度的报账
    dinner_date_map_name = {}

    for row_idx in range(dinner_row_start, dinner_row_end):
        row = table_by_name.row_values(row_idx)
        if row:
            name = row[name_col]
            dates = row[date_col]
            if name and dates:
                date_list = format_date_col(dates)
                for date in date_list:
                    date_mapped_name = dinner_date_map_name.get(date)
                    if not date_mapped_name:
                        date_mapped_name = []
                        dinner_date_map_name[date] = date_mapped_name
                    date_mapped_name.append(name)
            else:
                pass
            # print "name:%s, date:%s" % (row[name_col], row[date_col])

    # 按照日期排序
    sorted_map_list = sorted(dinner_date_map_name.iteritems(), key=lambda d:d[0],reverse=False)

    for i in sorted_map_list:
        date_str = i[0]
        name_list = i[1]
        print u"日期:%s, 报账人员:%s" % (date_str, cat_str_list(name_list))
        # print "date:%s, name_list:%s" % (date_str, json.dumps(name_list, encoding="UTF-8", ensure_ascii=False))

    need_write = False
    need_write = True
    if need_write:
        fpath, fext = os.path.splitext(excel_file)
        temp_excel_file = fpath + "-bydate." + fext
        write_excel = xlwt.Workbook()
        sheet = write_excel.add_sheet(u"按日期报账")
        idx = 0
        for i in sorted_map_list:
            date_str = i[0]
            name_list = i[1]
            sheet.write(idx, 0, date_str)
            sheet.write(idx, 1, cat_str_list(name_list))
            sheet.write(idx, 2, len(name_list))
            sheet.write(idx, 3, len(name_list) * 15)
            idx += 1
        write_excel.save(temp_excel_file)

