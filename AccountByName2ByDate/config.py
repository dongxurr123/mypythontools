# coding=utf-8
__author__ = 'dongxu'

# 加班餐报账的起始行
dinner_row_start = 2

# 加班餐报账的结束行
dinner_row_end = 21

# 打车费报账的起始行
taxi_row_start = 12

# 打车费报账的结束行
taxi_row_end = 14

# 人名列号
name_col = 0

# 日期列号
date_col = 2

# excel文件路径
excel_file = u"/export/Develop/Documents/报账明细/统一工作平台项目组报账明细_2016-05.xls"